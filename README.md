# Camera Software Installation
This document goes over how to install the software needed to interface with the camera.

## Avaris installation
Install dependencies.
```
sudo apt-get install autoconf intltool libxml2-dev python-gobject-dev gobject-introspection gtk-doc-tools libgstreamer0.10-dev python-gst0.10-dev
git clone https://github.com/AravisProject/aravis.git
cd aravis
./autogen.sh
./configure
make
sudo make install
sudo mv ./aravis.rules /etc/udev/rules.d
```

## Steve's code installation
Create catkin workspace
```
$ mkdir -p camera_ws/src
$ cd camera_ws
$ catkin_make
$ cd src
$ git clone https://github.com/mcguire-steve/camera_aravis.git
$ git clone https://github.com/ros-drivers/driver_common.git
$ cd ..
$ catkin_make
```

Source the package (temporary: will need to be done each time you open a new terminal session)
```
$  source devel/setup.bash
```
For a permenant solution just add the following line to your .bashrc
```
$ source <path_to_workspace>/camera_ws/devel/setup.bash
```
## Running the camera node
In first terminal
```
$ roscore
```
In second terminal (note: you may have to re-source if you did not add to the .bashrc)
```
$ rosrun camera_aravis camnode
```
You can preview the images by using image_view
```
$ sudo apt install ros-kinetic-image-view
$ rosrun image_view image_view image:=/aravis_camera/image_raw
```
The camnode.cpp file on the Jackal has been modified to force the full frame. This is on line 1064 where the ROI is manually set.
```
arv_camera_set_region (global.pCamera, global.xRoi, global.yRoi, global.widthRoi, global.heightRoi);
```
## Launch File Info
There are three launch files. 

export.launch exports raw image data from a ROS bag to PNG image files in a folder.

viewbag.launch allows user to view compressed or raw images from a ROS bag using image_viewer.

UItest.launch will start the camera ROS node which publishes (among other topics) the raw image topic. It
also starts a downsampling and compression node called "my_decimator" to publish the image topic that will be sent to the UI.
There is an optional dropframe node to throttle the number of image messages that get published. 
Finally, there is a rosbag node that collects the raw image data into a ROS bag.

Roslaunch files can be run from anywhere in the catkin workspace, but if they are not in the current directory you must specify 
the path to their location. The examples below assume you are in the directory containing the launch file.

*Note, that when specifying paths to directories, use the full path ie: /home/user/etc...
### UItest.launch
```
roslaunch UItest.launch saveloc:="path_to_directory_to_save_rosbag"
```

### viewbag.launch
```
roslaunch viewbag.launch bagfile=:"ROS_bag_to_view" topic:="Topic_within_bag_to_view" format:="raw/compressed"
```
Note that the format is either raw or compressed depending on which topic you are viewing.

### export.launch
```
roslaunch export.launch bagfile:="ROS_bag_to_view" topic:="Topic_within_bag_to_view" saveloc:="Path_to_location_to_write_image_files"
```
### Camera Calibration
Camera calibration is accomplished through the camera_calibration ROS package.

